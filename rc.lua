
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
--local menubar = require("menubar")
-- Vicious
local vicious = require("vicious")
-- Lain
local lain = require("lain")
-- freedesktop.org
local freedesktop = require('freedesktop')

local drop = require('scratchdrop')

hostname = io.popen("uname -n"):read()
wireless_interface = os.getenv("WIRELESS_INTERFACE") or "wlp3s0"
ethernet_interface = os.getenv("ETHERNET_INTERFACE") or "enp0s25"


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}
naughty.config.defaults.position         = "bottom_right"
-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init(awful.util.getdir("config") .. "/themes/cesious/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "termite"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {
  names  = { "1", "2", "3", "4", "5", "6", "7", "8", "9"},
  layout = { layouts[8], layouts[10], layouts[8], layouts[8], layouts[8], layouts[8], layouts[8], layouts[8], layouts[8]

}}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Menu
freedesktop.utils.terminal = terminal
menu_items = freedesktop.menu.new()
myawesomemenu = {
   { "manual", terminal .. " -e man awesome", freedesktop.utils.lookup_icon({ icon = 'help' }) },
   { "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua", freedesktop.utils.lookup_icon({ icon = 'package_settings' }) },
   { "edit theme", editor_cmd .. " " .. awful.util.getdir("config") .. "/themes/cesious/theme.lua", freedesktop.utils.lookup_icon({ icon = 'package_settings' }) },
   { "restart", awesome.restart, freedesktop.utils.lookup_icon({ icon = 'gtk-refresh' }) },
}
table.insert(menu_items, { "awesome", myawesomemenu, beautiful.awesome_icon })
table.insert(menu_items, { "open terminal", terminal, freedesktop.utils.lookup_icon({icon = 'terminal'}) })
mymainmenu = awful.menu({ items = menu_items, theme = { width = 150 } })
mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })
-- }}}

-- {{{ Wibox
markup      = lain.util.markup
darkblue    = theme.bg_focus
blue        = "#9EBABA"
red         = "#EB8F8F"

-- Separators
spacer = wibox.widget.textbox(" ")
seperator = wibox.widget.textbox(' <span color="' .. darkblue .. '">|</span> ')

-- Create a textclock widget
mytextclock = awful.widget.textclock("%d-%m-%y %H:%M", 5)
-- Show calendar when hovering over mytextclock
lain.widgets.calendar:attach(mytextclock, {position="bottom_right"})

cpu = wibox.widget.textbox()
vicious.register(cpu, vicious.widgets.cpu, '<span font="Icons" color="' .. blue .. '">' .. utf8.char(0xf35e) .. '</span> $1%', 2)

mem = wibox.widget.textbox()
vicious.register(mem, vicious.widgets.mem, '<span font="Icons" color="' .. blue .. '">'.. utf8.char(0xf320) .. '</span> $1% ($2MB)', 5)

netwidget = lain.widgets.net({ iface =wireless_interface, timeout = 1, notify = "off",
	settings = function()
          if net_now.state == "up" then
		widget:set_markup(markup(blue, '<span color="white"><span color="' .. blue .. '" font="Icons">' .. utf8.char(0xF251) .. '</span> ' .. net_now.received..' <span color="' .. blue .. '" font="Icons">' .. utf8.char(0xF255) .. '</span> ' ..net_now.sent .. '</span>'))
                else
                  widget:set_markup(markup(blue, '<span font="Icons">' .. utf8.char(0xF35C) .. utf8.char(0xF3D7) .. '</span>'))
                end
              end
            })

-- ALSA
vol = lain.widgets.alsa({
    settings = function()
        if volume_now.status == "off" then
            level = markup(red, volume_now.level .. "M")
        else
            level = volume_now.level .. "%"
        end
        widget:set_markup(markup(blue, "<span font=\"Icons\"> ".. utf8.char(0xF028) .. "</span> ") .. level)
    end
})
-- ALSA Mouse -- LeftClick=1 RightClick=3 ScrollUp=4 ScrollDown=5
vol:buttons(awful.util.table.join(
    awful.button({}, 1, function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh mute")
        vol.update()
    end),
    awful.button({}, 3, function ()
        awful.util.spawn(terminal .. " -e alsamixer")
        vol.update()
    end),
    awful.button({}, 4, function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh up")
        vol.update()
    end),
    awful.button({}, 5, function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh down")
        vol.update()
    end)
))

-- Battery
bat = lain.widgets.bat({
    battery = "BAT0",
    settings = function()
        if bat_now.perc == "N/A" then
            perc = ""
        elseif tonumber(bat_now.perc) <= 15 then
            perc = markup(red, bat_now.perc .. "%") .. " " .. bat_now.time
        else
            perc = bat_now.perc .. "%" .. " " .. bat_now.time
        end

        if bat_now.status == "Not present" then
          widget:set_markup(markup(red, '<span font="Icons">' .. utf8.char(0xF212) .. '</span> '))
        elseif bat_now.status == "Charging" then
          widget:set_markup(markup(blue, '<span font="Icons">' .. utf8.char(0xF211) .. '</span> ') .. perc)
          else
          widget:set_markup(markup(blue, '<span font="Icons">' .. utf8.char(0xF214) .. '</span> ') .. perc)
        end
    end
})

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "bottom", height = "18", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(spacer)
    left_layout:add(mytaglist[s])
    left_layout:add(spacer)
    left_layout:add(mylayoutbox[s])
    left_layout:add(spacer)
    left_layout:add(mypromptbox[s])
    left_layout:add(spacer)

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(spacer)
    right_layout:add(vol)
    right_layout:add(seperator)
    right_layout:add(cpu)
    right_layout:add(seperator)
    right_layout:add(mem)
    right_layout:add(seperator)
    right_layout:add(bat)
    right_layout:add(seperator)
    right_layout:add(netwidget)
    right_layout:add(seperator)
    right_layout:add(mytextclock)
    right_layout:add(seperator)
    if s == 1 then right_layout:add(wibox.widget.systray()) end

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),
    awful.key({ modkey,           }, "F9", function() drop("termite -e htop", "center",
	    "center", 0.8, 0.8) end),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
          awful.client.focus.byidx(1)
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn_with_shell(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
--    awful.key({ modkey, "Shift"   }, "q", awesome.quit), -- Default M-S-q command, oblogout lets you choose what to do
    awful.key({ modkey, "Shift"   }, "q", function () awful.util.spawn_with_shell("oblogout") end),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
--    awful.key({ modkey }, "p", function() menubar.show() end) -- Default M-p command, dmenu is a better/faster alternative
    awful.key({ modkey }, "p", function () awful.util.spawn_with_shell("dmenu_run ") end),


    -- Toggle wibox visibility
    awful.key({ modkey }, "b", function ()
     mywibox[mouse.screen].visible = not mywibox[mouse.screen].visible
    end), 

    -- Switch to specific layout
    awful.key({ modkey, "Control" }, "f", function () awful.layout.set(awful.layout.suit.floating) end),
    awful.key({ modkey, "Control" }, "t", function () awful.layout.set(awful.layout.suit.tile) end),
    awful.key({ modkey, "Control" }, "b", function () awful.layout.set(awful.layout.suit.tile.bottom) end),
    awful.key({ modkey, "Control" }, "s", function () awful.layout.set(awful.layout.suit.fair) end),
    awful.key({ modkey, "Control" }, "m", function () awful.layout.set(awful.layout.suit.max) end),

    -- Volume control (Script which uses 'volnoti' as notification)
    awful.key({ }, "XF86AudioRaiseVolume", function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh up")
        vol.update() end),
    awful.key({ }, "XF86AudioLowerVolume", function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh down")
        vol.update() end),
    awful.key({ }, "XF86AudioMute",        function ()
        awful.util.spawn(awful.util.getdir("config") .. "/scripts/volume.sh mute")
        vol.update() end),
    awful.key({ }, "XF86AudioPlay", function () awful.util.spawn_with_shell("deadbeef --play-pause") end),
    awful.key({ }, "XF86AudioNext", function () awful.util.spawn_with_shell("deadbeef --next") end),
    awful.key({ }, "XF86AudioPrev", function () awful.util.spawn_with_shell("deadbeef --prev") end),

    -- Brightness control (Script which uses 'volnoti' as notification) SYNTAX: up/down | percentage
    awful.key({ }, "XF86MonBrightnessUp",   function () awful.util.spawn_with_shell(awful.util.getdir("config") .. "/scripts/brightness.sh up") end),
    awful.key({ }, "XF86MonBrightnessDown", function () awful.util.spawn_with_shell(awful.util.getdir("config") .. "/scripts/brightness.sh down") end),
    awful.key({ }, "XF86MonBrightnessDown", function () awful.util.spawn_with_shell(awful.util.getdir("config") .. "/scripts/brightness.sh down") end),

    -- Turning off monitor.
    awful.key({ }, "XF86Launch1", function () awful.util.spawn_with_shell("xset dpms force off") end),

    -- Screen lock
    awful.key({ modkey, "Shift" }, "l", function () awful.util.spawn_with_shell("dm-tool lock") end),

    -- Screenshot
    awful.key({ }, "Print", function () awful.util.spawn_with_shell("scrot -e 'mv $f ~/Pictures/'") end),
    awful.key({ "Shift" }, "Print", function () awful.util.spawn_with_shell("scrot -s -e 'mv $f ~/Pictures/'") end),

    -- Applications
    awful.key({ modkey }, "F9", function () awful.util.spawn_with_shell("termite -geometry 90x40 --title htop -e htop") end)
)

if hostname == 'sysiakowy' then
    globalkeys = awful.util.table.join(
	    globalkeys, awful.key({ modkey}, "g", function ()
		awful.util.spawn_with_shell("google-chrome-stable") end),
	    globalkeys, awful.key({ modkey}, "F10", function ()
		awful.util.spawn_with_shell("xbacklight -inc 10") end),
	    globalkeys, awful.key({ modkey}, "F11", function ()
		awful.util.spawn_with_shell("xbacklight -dec 10") end)
	    )
end

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.movetotag(tag)
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.toggletag(tag)
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     size_hints_honor = false, -- Remove gaps between terminals
                     callback = awful.client.setslave } }, -- Open new clients as slave instead of master
    { rule_any = { class = {"MPlayer", "Oblogout", "qmlscene"},
                   name = {"htop"},
                   instance = {"plugin-container", "exe"} },
                   properties = { floating = true } },
    { rule_any = { class = {"google-chrome"} }, properties = {tag = tags[1][2]} },
    { rule_any = { class = { "Deluge", "Flareget" } },
      properties = { tag = tags[1][8] } },
    { rule = { class = "Deluge", name = "Dodaj torrenty" },
      callback = move_to_current_tag },
}

awful.rules.sysiakowe = {
    { rule_any = { class = {"Firefox"} }, properties = {tag = tags[1][9]} },
    { rule_any = { class = {"Deadbeef"} }, properties = {tag = tags[1][8]} },
    { rule_any = { class = {"Pcmanfm"} }, properties = {tag = tags[1][3]} }
}

awful.rules.default = {
    { rule_any = { class = {"Deadbeef"} }, properties = {tag = tags[1][9]} }
}

if hostname == 'sysiakowy' then
awful.rules.rules = awful.util.table.join(awful.rules.rules, awful.rules.sysiakowe)
    else
awful.rules.rules = awful.util.table.join(awful.rules.rules, awful.rules.default)
    end
-- }}}

move_to_current_tag = function(c)
    awful.client.movetotag(tags[mouse.screen][awful.tag.getidx()], c)
end

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

for s = 1, screen.count() do
    screen[s]:connect_signal("arrange", function ()
        local clients = awful.client.visible(s)
        local layout  = awful.layout.getname(awful.layout.get(s))
        if layout ~= awful.layout.suit.max or #clients > 1 then
            for _, c in pairs(clients) do -- Floaters always have borders
                    c.border_width = beautiful.border_width
                    c.border_color = beautiful.border_focus
                end
              end
    end)
end


-- {{{ Disable startup-notification globally
local oldspawn = awful.util.spawn
awful.util.spawn = function (s)
    oldspawn(s, false)
end
-- }}}

-- {{{ Run autostarting applications only once
function autostart(cmd, delay)
    delay = delay or 0
    awful.util.spawn_with_shell("pgrep -u $USER -x -f '" .. cmd .. "' || ( sleep " .. delay .. " && " .. cmd .. " )")
end

-- Autostart applications. The extra argument is optional, it means how long to
-- delay a command before starting it (in seconds).
-- autostart("volnoti -t 1", 1)
autostart("nm-applet")
autostart("setxkbmap pl", 1)
if hostname == 'sysiakowy' then
    autostart("pcmanfm")
    autostart("google-chrome-stable")
end
if hostname == 'x230' then
    autostart("redshift-gtk")
end
