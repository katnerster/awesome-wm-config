#!/bin/bash
cp wall.jpg ~/
git clone https://github.com/terceiro/awesome-freedesktop.git
mv ./awesome-freedesktop/freedesktop ./
rm -rf ./awesome-freedesktop
mv speedup.patch ./freedesktop
cd ./freedesktop
patch < speedup.patch

mv ../init.lua ./
